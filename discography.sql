-- 
-- Table structure for table discography
-- 

CREATE TABLE `discography` (
  `nid` int(10) unsigned NOT NULL default '0',
  `year` int(4) unsigned default '0',
  `artist` varchar(128) default NULL,
  `label` varchar(32) default NULL,
  `ITMS` varchar(128) default NULL,
  `tracks` longtext,
  `ASIN` varchar(16) default NULL,
  `Cassette` tinyint(1) default NULL,
  `CD` tinyint(1) default NULL,
  `DVD` tinyint(1) default NULL,
  PRIMARY KEY  (`nid`)
) TYPE=MyISAM;

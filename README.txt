Discography 4.6
===============

Allows sites to list a albums arranged by artist name.

REQUIRES Amazon_Items module.

This module defines a new node type called discography, which contains album details. You can enter as much information as you wish for an album, including optional track listing and a description. If you enter an Amazon item number (ASIN), purchase links for Amazon will be shown. You can also enter iTunes Music Store links.

See this module in action at http://www.worldbeatplanet.com/discography

Discography provides three different list views, as well as an album detail view.

1) http://www.example.com/discography - displays only artist names as clickable links
2) http://www.example.com/discography/all - displays albums for all artists
3) http://www.example.com/discography/artist+name - displays albums only for the specified artist

INSTALLATION:
============

1. Make sure amazon_items is installed along with the required include files, AmazonProducts.class.inc and AmazonSearchEngine.class.inc. If you have an Amazon affiliate ID, make sure you enter it in amazon_items settings.

2. Copy this entire folder to your modules directory.

3. Create the table using the included sql file.
    (mysql -u user -p database <discography.sql)

4. Enable discography in admin/modules.

If you wish, you can replace the default icons included with this module with alternate icons for cd, cassette, amazon links, or iTunes links.

Enjoy!

Mike Cohen
mike3k@gmail.com
http://www.mcdevzone.com/
